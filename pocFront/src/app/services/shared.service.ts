import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class SharedService {

  public static instance: SharedService = null;
  token: string;
  showTemplate = new EventEmitter<boolean>();

  constructor() {
    return SharedService.instance = SharedService.instance || this;
  }

  public static getInstance() {
    if (this.instance == null) {
      this.instance = new SharedService();
    }
    return this.instance;
  }

  isLoggedIn(): boolean {
    if (this.token == null) {
      return false;
    }
    return this.token != '';
  }

}
