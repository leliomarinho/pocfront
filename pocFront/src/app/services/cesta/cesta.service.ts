import { API_BACKEND } from '../pocfront.api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable()
export class CestaService {
  constructor(private http: HttpClient) { }

  findByNome(nome: string, filial: number, ordemPreco: boolean): Observable<any>{
    
    return this.http.get(`${API_BACKEND}/item/v3/itens/base/autocomplete?nome=${nome}&codigoFilial=${filial}&maxResult=200&ordenarRentabilidade=true&ordenarPreco=${ordemPreco}`);
  }

}



