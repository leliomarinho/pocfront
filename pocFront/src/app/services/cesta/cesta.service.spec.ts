import { CestaService } from './cesta.service';

describe('Dado que Validando CestaService', () => {
    let service: CestaService;
    // beforeEach(() => { service = new CestaService(); });

    it('#Quando buscar (Get) por nome findBynome', () => {
        expect(service.findByNome()).toBe('real value');
    });

    it('#Então retorna uma lista de com o nome',
        (done: DoneFn) => {
            service.getObservableValue().subscribe(value => {
                expect(value).toBe('observable value');
                done();
            });
        });

    it('#getPromiseValue should return value from a promise',
        (done: DoneFn) => {
            service.getPromiseValue().then(value => {
                expect(value).toBe('promise value');
                done();
            });
        });
});