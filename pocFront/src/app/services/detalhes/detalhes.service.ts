import { Detalhe } from './../../model/detalhe';
import { API_BACKEND } from './../pocfront.api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class DetalharService {

  constructor(private http: HttpClient) { }

  findByDetalhes(detalhe: Detalhe): Observable<any> {
    
    // let postData = {
    //   filial: "101",
    //   perfil: 1,
    //   consultaRegrasFiscais: { uf: "RS", pais: "BR", ufDestino: "RS", paisDestino: "BR"  },
    //   itens: [{ codigo: 896760, quantidade: 1}]
    // };
    

    return this.http.post(`${API_BACKEND}/mostruario/v3/itens/detalhe`, detalhe);
  }


}
