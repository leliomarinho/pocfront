import { HttpClient } from '@angular/common/http';
import { Detalhe } from './../../model/detalhe';
import { API_BACKEND } from './../pocfront.api';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class MostruarioService {

  constructor(private http: HttpClient) { }

  findPreco(filial: number, itens: string): Observable<any> {
    return this.http.get(`${API_BACKEND}/mostruario/v3/itens/precos?filial=${filial}&perfil=1${itens}`);
  }





}
