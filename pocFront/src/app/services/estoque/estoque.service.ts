import { API_BACKEND } from './../pocfront.api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class EstoqueService {

  constructor(private http: HttpClient) { }

  findEstoque(filial: number, itens: string): Observable<any> {
    return this.http.get(`${API_BACKEND}/filial/v1/filiais/${filial}/estoque?${itens}`);
  }



}
