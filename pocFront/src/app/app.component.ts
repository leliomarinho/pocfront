import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { SharedService } from './services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  showTemplate: boolean = false;
  public shared: SharedService;
  
  constructor(private router: Router){
    this.shared = SharedService.getInstance();
  }

  ngOnInit(){
    this.shared.showTemplate.subscribe(
      show => this.showTemplate = show
    );
    this.login();
  }

  showContentWrapper(){
    return {
      'content-wrapper': this.shared.isLoggedIn()
    }
  }

  login() {
    this.shared.token = "mCl6SnTQp6eT";
    this.shared.showTemplate.emit(true);
    this.router.navigate(['/']);
  }

}