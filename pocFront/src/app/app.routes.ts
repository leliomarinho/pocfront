import { CestaListComponent } from './components/cesta-list/cesta-list.component';
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './components/home/home.component';
import { ModuleWithProviders } from "@angular/core";
import { AuthGuard } from './components/security/auth.guard';

export const ROUTES: Routes = [
  { path: '' , component:  HomeComponent, canActivate: [AuthGuard]},
  { path: 'cesta-list', component: CestaListComponent, canActivate: [AuthGuard] },

]

export const routes: ModuleWithProviders = RouterModule.forRoot(ROUTES);

