import { RegrasFiscais } from './regrasFiscais';
import { Itens } from './itens';
export class Detalhe {
    constructor( public filial: string,
                public perfil: number,
                public itens: Array<Itens> = [],
                public consultaRegrasFiscais: RegrasFiscais = new RegrasFiscais('','','','')
            )
        {

        }
}
