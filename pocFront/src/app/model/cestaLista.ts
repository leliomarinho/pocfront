export class CestaLista {
    constructor(
        public codigo: string,
        public estoque: number,
        public preco: number,
        public descricao: string,
        public ean: string

    ) {

    }
}
