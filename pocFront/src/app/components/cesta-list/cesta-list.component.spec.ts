import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CestaListComponent } from './cesta-list.component';

describe("Dada uma string 'foo'", function () {
  var someString;
  beforeEach(function () {
    someString = "foo";
  });
  describe("Quando Adiciono um bar", function () {
    beforeEach(function () {
      someString += "bar";
    });

    it("Então a string será 'foobar'", function () {
      expect(someString).toBe("foobar");
    });
  });
 
});