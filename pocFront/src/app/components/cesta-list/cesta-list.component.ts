import { Observable, Subject } from 'rxjs';
import { CestaLista } from './../../model/cestaLista';
import { EstoqueService } from './../../services/estoque/estoque.service';
import { Itens } from './../../model/itens';
import { Detalhe } from './../../model/detalhe';
import { DetalharService } from './../../services/detalhes/detalhes.service';
import { CestaService } from '../../services/cesta/cesta.service';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';
import { RegrasFiscais } from '../../model/regrasFiscais';
import { MostruarioService } from '../../services/mostruario/mostruario.service';
import { debounceTime, map, mergeMap } from 'rxjs/operators';
import { forkJoin } from "rxjs/observable/forkJoin"

@Component({
  selector: 'app-cesta-list',
  templateUrl: './cesta-list.component.html'
})
export class CestaListComponent implements OnInit {
  nome: string;
  filial: number;
  ordemPreco: boolean
  shared: SharedService;
  message: {};
  classCss: {};
  listaItens: any;
  listaEstoque: any;
  listaPrecos: any;
  listaDetalhes: any;
  cestaLista: Array<CestaLista>;
  codigoItem: string;
  detalhe = new Detalhe('', 0, null, null);
  itemDetalhe = new Itens(null,null);
  regrasFiscais = new RegrasFiscais('','','','');
  listaCompleta: any;

  constructor(
    private cestaService: CestaService,
    private detalhesService: DetalharService,
    private estoqueService: EstoqueService,
    private mostruarioService: MostruarioService


     ) {
    this.shared = SharedService.getInstance();
  }

  ngOnInit() {

  }

  pesquisarNome(): void {


    this.cestaService.findByNome(this.nome, this.filial, this.ordemPreco)
      
      .subscribe(reponseItens => {

        this.listaItens = reponseItens;

        var codigoItemPreco = '';
        var codigoItemEstoque = '';

        this.listaItens.forEach(element => {

          codigoItemPreco = codigoItemPreco + "&item=" + element.codigoItem
          codigoItemEstoque = codigoItemEstoque + "&itens=" + element.codigoItem

        });

        // this.pesquisarPrecoEstoque(codigoItemPreco, codigoItemEstoque);

        var precos = this.mostruarioService.findPreco(this.filial, codigoItemPreco);
        var estoque = this.estoqueService.findEstoque(this.filial, codigoItemEstoque);

        forkJoin([precos, estoque]).subscribe(results => {

          this.listaPrecos = results[0];

          this.listaEstoque = results[1];

        });

        this.cestaLista = new Array<CestaLista>();

        this.listaItens.forEach(item => {

          let estoque = this.listaEstoque.filter(x => x.codigoItem == item.codigoItem);

          let precos = this.listaPrecos.filter(x => x.codigoItem == item.codigoItem);

          var itemcesta = new CestaLista('', null, null, '', '');

          itemcesta.codigo = item.codigoItem;
          itemcesta.descricao = item.nomenclaturaVarejo;
          itemcesta.estoque = estoque[0].estoqueLoja
          itemcesta.preco = precos[0].preco.precoVenda;

          this.cestaLista.push(itemcesta);

        });

      }, err => {
        this.showMessage({
          type: 'error',
          text: err['error']['status'] + " - " + err['error']['error']
        });
      });
   


  }

  pesquisarPrecoEstoque(codigoItemPreco: string, codigoItemEstoque: string): void{
    
    var precos = this.mostruarioService.findPreco(this.filial, codigoItemPreco);
    var estoque = this.estoqueService.findEstoque(this.filial, codigoItemEstoque);

    forkJoin([precos, estoque]).subscribe(results => {

      this.listaPrecos = results[0];

      this.listaEstoque = results[1];

    });
    
  }

  private showMessage(message: { type: string, text: string }): void {
    this.message = message;
    this.buildClasses(message.type);
    setTimeout(() => {
      this.message = undefined;
    }, 3000);
  }

  private buildClasses(type: string): void {
    this.classCss = {
      'alert': true
    }
    this.classCss['alert-' + type] = true;
  }

  detalhar(codigo: number): void {

    this.detalhe = new Detalhe('',null,[]);
    
    this.regrasFiscais = new RegrasFiscais('', '', '', '');

    this.itemDetalhe.codigo = codigo;
    this.itemDetalhe.quantidade = 1;

    this.detalhe.itens.push(this.itemDetalhe);
      
    this.detalhe.filial = this.filial.toString();
    this.detalhe.perfil = 1;

    this.detalhe.consultaRegrasFiscais.pais = 'BR';
    this.detalhe.consultaRegrasFiscais.paisDestino = 'BR';
    this.detalhe.consultaRegrasFiscais.uf = 'RS';
    this.detalhe.consultaRegrasFiscais.ufDestino = 'RS';

    this.detalhesService.findByDetalhes(this.detalhe).subscribe(responseDetalhe => {

      this.listaDetalhes = responseDetalhe['itens'];

      console.log(this.listaDetalhes );

    }, err => {
      this.showMessage({
        type: 'error',
        text: err['error']['status'] + " - " + err['error']['error']
      });
    });


  }
  
  

 


}
