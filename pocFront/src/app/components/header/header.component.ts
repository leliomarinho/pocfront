import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  
  public shared: SharedService;

  constructor(private router: Router){
      this.shared = SharedService.getInstance();
  }

  ngOnInit(){
  }

}
